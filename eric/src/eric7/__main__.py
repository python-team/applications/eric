# -*- coding: utf-8 -*-

# Copyright (c) 2022 - 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Main script to enable starting eric7 with 'python -m eric7'.
"""

from . import eric7_ide

eric7_ide.main()
