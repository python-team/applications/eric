# -*- coding: utf-8 -*-

# Copyright (c) 2020 - 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Package implementing the security checker.
"""

###########################################################################
## The security checker is based on Bandit v1.7.8.                       ##
###########################################################################
