# -*- coding: utf-8 -*-

# Copyright (c) 2006 - 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Package implementing some special widgets.

They extend or amend the standard widgets as found in QtWidgets.
"""
